import  paho.mqtt.client
import json
import time

# --- CONFIG/CONST --- #

MY_IP = "127.0.0.1"
MY_PORT = 1883

WEB_SERVER_IP = "172.19.16.18"
WEB_SERVER_PORT = 1883

BUFFER_IP = "172.19.16.19"
BUFFER_PORT = 1883

QoS=2
###

client_web = paho.mqtt.client.Client("WEB")
client_web.connect(WEB_SERVER_IP, WEB_SERVER_PORT)
client_web.loop_start()

client_buffer = paho.mqtt.client.Client("BUFFER")
client_buffer.username_pw_set("event_center", "event_center")
client_buffer.connect(BUFFER_IP, BUFFER_PORT)
client_buffer.loop_start()

def on_message(client, userdata, message):
	print("["+message.topic+"]" + " - "+ str(message.qos) + " - " + message.payload.decode("utf-8"))
	client_web.publish("EVENT", message.payload.decode("utf-8"), qos=QoS)
	client_buffer.publish("EVENT", message.payload.decode("utf-8"), qos=QoS)


client_broker = paho.mqtt.client.Client("BROKER")
client_broker.on_message=on_message
client_broker.connect(MY_IP, MY_PORT)
client_broker.loop_start()
client_broker.subscribe("EVENT")


try:
	while True:
		time.sleep(0.1)
except KeyboardInterrupt:
        client_broker.disconnect()
        client_broker.loop_stop()
	client_web.disconnect()
	client_web.loop_stop()
	client_buffer.disconnect()
	client_buffer.loop_stop()

