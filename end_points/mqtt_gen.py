import random
import time
import paho.mqtt.client
import json

#  --- CONFIG/CONST ---  #

PASSERELLE_IP="172.19.1.4"
PASSERELLE_PORT=1883
QoS=0

STATION_ID=1
STATION_TYPE=5

USERNAME="auto"

#permet de limiter la vitesse si c'est un camion
CAMION=False

LOW_FREQ=1
HIGH_FREQ=10

START_SPEED=15
START_GPS={
	"N": 48.8,
	"E": 2.2
	}
START_HEADING=0.0

# event config
#probabilité de proc de l'event
TRAVAUX = 0.25
ACCIDENT = 0.25
EMBOUTEILLAGE = 0.25
ROUTE_GLISSANTE = 0.25
BROUILLARD = 0.25

###


#genere une nouvelle vitesse
def new_speed(actual_speed):
	v=random.gauss(actual_speed,20)
	if CAMION:
		return max(0,min(v, 90))
	return max(0, min(v, 130))

vitesse=START_SPEED
gps=START_GPS
heading=START_HEADING
client = paho.mqtt.client.Client(USERNAME)
client.connect(PASSERELLE_IP, PASSERELLE_PORT)
client.loop_start()


data_DENM={
	"stationId": STATION_ID,
	"stationType": STATION_TYPE,
	"code": 0,
	"subcode": 0,
	"GPS": gps
}

data_CAM={
	"staionId": STATION_ID,
	"stationType": STATION_TYPE,
	"vitesse": vitesse,
	"Heading": heading,
	"GPS": gps
}

try:
	while True:
		#on change notre vitesse
		vitesse=new_speed(vitesse)
		print(vitesse)
		
		#gestion de la frequence d'emission par rapport a la vitesse
		if vitesse >= 90:
			freq=HIGH_FREQ
		else:
			freq=LOW_FREQ
		time.sleep(1/freq)
		
		#envoi des donnée par MQTT
		data_CAM={
			"stationId": STATION_ID,
			"stationType": STATION_TYPE,
			"vitesse": vitesse,
			"Heading": heading,
			"GPS": gps 
		}
		client.publish("CAM", json.dumps(data_CAM), qos=QoS)

		if random.uniform(0,1) <= TRAVAUX:
			data_DENM["code"] = 3
			data_DENM["subcode"] = 0	
			client.publish("DENM", json.dumps(data_DENM), qos=QoS)

		if random.uniform(0,1) <= ACCIDENT:
			data_DENM["code"] = 4
			data_DENM["subcode"] = 0
			client.publish("DENM", json.dumps(data_DENM), qos=QoS)

		if random.uniform(0,1) <= EMBOUTEILLAGE:
			data_DENM["code"] = 5
			data_DENM["subcode"] = 0
			client.publish("DENM", json.dumps(data_DENM), qos=QoS)

		if random.uniform(0,1) <= ROUTE_GLISSANTE:
			data_DENM["code"] = 6
			data_DENM["subcode"] = 0
			client.publish("DENM", json.dumps(data_DENM), qos=QoS) 

		if random.uniform(0,1) <= BROUILLARD:
			data_DENM["code"] = 7
			data_DENM["subcode"] = 0
			client.publish("DENM", json.dumps(data_DENM), qos=QoS)

except KeyboardInterrupt:	
	client.disconnect()
	client.loop_stop()
