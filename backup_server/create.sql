DROP DATABASE IF EXISTS srv_backup;
CREATE DATABASE srv_backup;
GRANT INSERT ON srv_backup.* TO 'srv_backup'@'localhost' IDENTIFIED BY 'srv_backup';
GRANT SELECT ON srv_backup.* TO 'srv_web'@'%' IDENTIFIED BY 'srv_web';
FLUSH privileges;
DROP TABLE IF EXISTS srv_backup.event;
CREATE TABLE srv_backup.event(
	time TIMESTAMP,
	code INT(2),
	gps_n DOUBLE,
	gps_e DOUBLE
);
