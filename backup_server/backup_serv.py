import json
import pika
import mysql.connector
#  --- CONFIG/CONST ---  #

BUFFER_IP = "172.19.32.34"
MY_IP = "127.0.0.1"

###

mysql_bdd = mysql.connector.connect(
	host=MY_IP,
	user="srv_backup",
	password="srv_backup",
	database="srv_backup"
)

sql_insert = "INSERT INTO customers (time, code, gps_n, gps_e) VALUES (%s, %s, %s, %s)"


def backup(ch, method, properties, body):
	print("EVENT - "+str(body))
	data = json.loads(body)
	mysql_bdd.cursor().execute(sql_insert, (data["time"], data["code"], data["gps"]["N"], data["gps"]["E"]))
	mysql_bdd.commit()
	

login = pika.PlainCredentials("event_center", "event_center")
connection = pika.BlockingConnection(pika.ConnectionParameters(host=BUFFER_IP, credentials=login))
channel = connection.channel()
channel.queue_declare(queue="EVENT")
channel.queue_bind(exchange="amq.topic", queue="EVENT", routing_key="EVENT")
channel.basic_consume(queue="EVENT", on_message_callback=backup)
channel.start_consuming()

while True:
	time.sleep(1)
