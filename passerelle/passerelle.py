import  paho.mqtt.client
import json
import time

# --- CONFIG/CONST --- #

MY_IP = "127.0.0.1"
MY_PORT = 1883
EVENT_CENTER_IP = "172.19.2.2"
EVENT_CENTER_PORT = 1883
QoS=2

SPEED_ALERT = 30

GPS_ACCURACY = 0.0001
###
messages_CAM = {}
messages_DENM = {}
msg_id = 0


def on_message(client, userdata, message):
	global msg_id
	global messages_CAM
	global messages_DENM
	#print("["+message.topic+"]" + " - "+ str(message.qos) + " - " + message.payload.decode("utf-8"))
	msg_id+=1
	if message.topic == "CAM":
		messages_CAM[str(msg_id)]={
			"time": time.time(),
			"message": json.loads(message.payload.decode("utf-8"))
		}
	if message.topic == "DENM":
		messages_DENM[str(msg_id)]={
			"time": time.time(),
			"message": json.loads(message.payload.decode("utf-8"))
		}


def gps_in(gps, events):
	for e in events:
		if abs(e["GPS"]["N"] - gps["N"]) <= GPS_ACCURACY and abs(e["GPS"]["E"] - gps["E"]) <= GPS_ACCURACY:
			return True
	return False


client = paho.mqtt.client.Client("BROKER")
client.on_message=on_message
client.connect(MY_IP, MY_PORT)
client.loop_start()
client.subscribe("CAM")
client.subscribe("DENM")

client_2 = paho.mqtt.client.Client("Passerelle")
client_2.connect(EVENT_CENTER_IP, EVENT_CENTER_PORT)
client_2.loop_start()

events_CAM = []
events_DENM = []

try:
	while True:
		#on retire les messages de + de 2min des messages CAM
		keys = list(messages_CAM.keys())
		for e in keys:
			if time.time() - messages_CAM[e]["time"] > 2*60:
				messages_CAM.pop(e)
		#on retire les evenement de + de 2min
		for e in range(0, len(events_CAM)):
			if time.time() - events_CAM[e]["time"] > 2*60:
				events_CAM.pop(e)
		
		# on cherche les embouteillages
		#on prend pour chaque véhicule sa vitesse minimal  des véhivule à moins de 30km/h et les coordonnée du moment
		slow_vehicles = {}
		keys = list(messages_CAM.keys())
		for e in keys:
			stationId = messages_CAM[e]["message"]["stationId"]
			if (not (stationId in list(slow_vehicles.keys())) or slow_vehicles[stationId]["vitesse"] > messages_CAM[e]["message"]["vitesse"]) and messages_CAM[e]["message"]["vitesse"] < SPEED_ALERT :
				slow_vehicles[stationId] = {
					"vitesse": messages_CAM[e]["message"]["vitesse"],
					"GPS": messages_CAM[e]["message"]["GPS"]	
				}

		#on cherche des véhicules proches dans les véhicules lents pour chercher les embouteillages
		new_events_CAM = []
		if len(slow_vehicles) >= 3:
			keys = list(slow_vehicles)
			for e in keys:
				neighbour = [slow_vehicles[e]]
				for i in keys:
					if e == i:
						continue
					if abs(slow_vehicles[e]["GPS"]["N"] - slow_vehicles[i]["GPS"]["N"]) < GPS_ACCURACY and abs(slow_vehicles[e]["GPS"]["E"] - slow_vehicles[i]["GPS"]["E"]) < GPS_ACCURACY :
						neighbour.append(slow_vehicles[i])
				#si >= à 3 véhicules -> embouteillages
				if len(neighbour) >= 3:
					#on ajoute un event, on lui donne un point GPS
					N=0
					E=0
					for i in range(0, len(neighbour)):
						N+=neighbour[i]["GPS"]["N"]
						E+=neighbour[i]["GPS"]["E"]
					N/=len(neighbour)
					E/=len(neighbour)
					gps = {
                                                "N": N,
                                                "E": E
                                        }
					
					#on vierifie si on ne connais pas déjà l'événement
					if not gps_in(gps, new_events_CAM) and not gps_in(gps, events_CAM):
						new_events_CAM.append({
							"time": time.time(),
							"code": 5,
							"GPS": gps
						})
					
		for e in new_events_CAM:
			client_2.publish("EVENT", json.dumps(e), qos=QoS)
			print("CAM - EVENT - "+str(e["code"]))
			events_CAM.append(e)

		#traitement des DENM
		#on supprime les events DENM de + de 10min
		keys = list(messages_DENM.keys())
		for e in keys:
			if time.time() - messages_DENM[e]["time"] > 10*60:
				messages_DENM.pop(e)

		#on retire les evenement de + de 10min
		for e in range(0, len(events_DENM)):
			if time.time() - events_EDNM[e]["time"] > 10*60:
				events_EDNM.pop(e)


		new_events_DENM = []
		keys = list(messages_DENM.keys())
		for e in keys:
			#si c'est un operateur
			message = messages_DENM[e]["message"]
			if message["stationType"] in [10,15] and gps_in(message["GPS"], new_events_DENM) and gps_in(message["GPS"], events_DENM):
				new_events_DENM.append({
					"time": time.time(),
					"code": message["code"],
					"GPS": message["GPS"]
				})
			else:
				neighbour = [messages_DENM[e]]		
				for i in keys:
					if i == e:
						continue
					message2 = messages_DENM[i]["message"]
					if abs(message2["GPS"]["N"] - message["GPS"]["N"]) < GPS_ACCURACY and abs(message["GPS"]["E"] - message["GPS"]["E"]) < GPS_ACCURACY and message["code"] == message2["code"] and message["stationId"] == message2["stationId"]:
						neighbour.append(messages_DENM[i])
				#si suffisament de vehicules detecte l'eventement
				if len(neighbour) >= 2:
					#on ajoute un event, on lui donne un point GPS
					N=0
					E=0
					for i in range(0, len(neighbour)):
						N+=neighbour[i]["message"]["GPS"]["N"]
						E+=neighbour[i]["message"]["GPS"]["E"]
					N/=len(neighbour)
					E/=len(neighbour)
					gps = {
						"N": N,
						"E": E
					}
					#on vierifie si on ne connais pas déjà l'événement
					if gps_in(gps, new_events_DENM) or gps_in(gps, events_DENM):
						new_events_DENM.append({
							"time": time.time(),
							"code": neighbour[0]["message"]["code"],
							"GPS": gps
						})
					
		for e in new_events_DENM:
			client_2.publish("EVENT", e, qos=QoS)
			print("DENM - EVENT - "+str(e["code"]))
			events_DENM.append(e)


except KeyboardInterrupt:
	client.disconnect()
	client.loop_stop()
	client2.disconnect()
	client2.loop_stop()

